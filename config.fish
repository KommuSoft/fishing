set PATH $PATH /home/kommusoft/cmds
set ORGN /home/kommusoft/organizer/
set SCRP /home/kommusoft/scripts/
set DARY /home/kommusoft/diary/

set le (echo -e "\e[0m")              # end all
set lmb (echo -e "\e[01;31;5m")       # begin blinking
set lmd (echo -e "\e[01;31m")         # begin bold
set lso (echo -e "\e[01;44;33m")      # begin standout-mode - info box
set lus (echo -e "\e[4;7m")           # begin underline

set -xU LESS_TERMCAP_mb "$lmb"      # begin blinking
set -xU LESS_TERMCAP_md "$lmd"      # begin bold
set -xU LESS_TERMCAP_me "$le"       # end mode
set -xU LESS_TERMCAP_se "$le"       # end standout-mode
set -xU LESS_TERMCAP_so "$lso"      # begin standout-mode - info box
set -xU LESS_TERMCAP_ue "$le"       # end underline
set -xU LESS_TERMCAP_us "$lus"      # begin underline
