function gitaca
    git add .
    if count $argv > /dev/null
        git commit --amend -S -am $argv
    else
        git commit --amend -S -am (git log --format="%s" -n 1)
    end
end
