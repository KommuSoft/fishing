function tlynx
	if count $argv > /dev/null
		torsocks lynx -force-secure $argv
	else
		torsocks lynx -force-secure 'https://duckduckgo.com/'
	end
end
