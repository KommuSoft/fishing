function tsearch
    if count $argv > /dev/null
        torsocks lynx -child -force-secure 'https://duckduckgo.com/?kac=1&k1=-1&kaf=1&q='(urlencode $argv); 
    else
        torsocks lynx -child -force-secure 'https://duckduckgo.com/'
    end
end
